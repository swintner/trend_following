import numpy as np


class SMA_VectorBacktester(object):
    def __init__(self, SMA1, SMA2, tc):
        # self.symbol = symbol
        # self.start = start
        # self.end = end
        self.SMA1 = SMA1
        self.SMA2 = SMA2
        self.tc = tc
        self.get_data()

    def get_data(self):
        raw = pd.read_csv('aapl.csv', index_col=0, parse_dates=True)
        # raw = raw['Close']
        raw['SMA1'] = raw['Close'].rolling(self.SMA1).mean()
        raw['SMA2'] = raw['Close'].rolling(self.SMA2).mean()
        raw['Returns'] = np.log(raw['Close'] / raw['Close'].shift(1))
        # raw = raw[['Close', 'SMA1', 'SMA2', 'Returns']]
        self.data = raw.dropna()

    def plot_data(self):
        self.data[['Close', 'SMA1', 'SMA2']].plot(figsize=(14, 8), title='AAPL')

    def run_strategy(self):
        data = self.data.copy()
        data['Position'] = np.where(data['SMA1'] > data['SMA2'], 1, -1)
        data['Strategy'] = data['Position'].shift(1) * data['Returns']
        # drop first row which had NaN
        data.dropna(inplace=True)
        # calculate trade
        trades = (data['Position'].diff().fillna(0) != 0)
        # update strategy for transaction costs
        data['Strategy'] = np.where(trades, data['Strategy'] - self.tc, data['Strategy'])
        data['CReturns'] = data['Returns'].cumsum().apply(np.exp)
        data['CStrategy'] = data['Strategy'].cumsum().apply(np.exp)
        self.results = data
        return data[['CReturns', 'CStrategy']].iloc[-1]

    def plot_results(self):
        self.results[['CReturns', 'CStrategy', 'Position']].apply(np.exp).plot(
            secondary_y='Position' ,figsize=(14, 8))