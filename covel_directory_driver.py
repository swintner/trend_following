def covel_directory_driver(market_symbol):
    if market_symbol.lower() == 'australian_dollar':
        return 'data/historical/australian_dollar/'
    if market_symbol.lower() == 'british_pound':
        return 'data/historical/british_pound/'
    if market_symbol.lower() == 'cac40':
        return 'data/historical/cac40/'
    if market_symbol.lower() == 'coffee':
        return 'data/historical/coffee/'
    if market_symbol.lower() == 'corn':
        return 'data/historical/corn/'
    if market_symbol.lower() == 'cotton':
        return 'data/historical/cotton/'
    if market_symbol.lower() == 'crb_index':
        return 'data/historical/crb_index/'
    if market_symbol.lower() == 'crude':
        return 'data/historical/crude/'
    if market_symbol.lower() == 'dollar_index':
        return 'data/historical/dollar_index/'
    if market_symbol.lower() == 'euro_dollar':
        return 'data/historical/euro_dollar/'
    if market_symbol.lower() == 'feeder_cattle':
        return 'data/historical/feeder_cattle/'
    if market_symbol.lower() == 'gilt':
        return 'data/historical/gilt/'
    if market_symbol.lower() == 'gold':
        return 'data/historical/gold/'
    if market_symbol.lower() == 'heating_oil':
        return 'data/historical/heating_oil/'
    if market_symbol.lower() == 'hg_copper':
        return 'data/historical/hg_copper/'
    if market_symbol.lower() == 'live_hogs':
        return 'data/historical/live_hogs/'
    if market_symbol.lower() == 'oats':
        return 'data/historical/oats/'
    if market_symbol.lower() == 'orange_juice':
        return 'data/historical/orange_juice/'
    if market_symbol.lower() == 'palladium':
        return 'data/historical/palladium/'
    if market_symbol.lower() == 'platinum':
        return 'data/historical/platinum/'
    if market_symbol.lower() == 'pork_bellies':
        return 'data/historical/pork_bellies/'
    if market_symbol.lower() == 'silver':
        return 'data/historical/silver/'
    if market_symbol.lower() == 'soy':
        return 'data/historical/soy/'
    if market_symbol.lower() == 'sp500':
        return 'data/historical/sp500/'
    if market_symbol.lower() == 'sterling':
        return 'data/historical/sterling/'
    if market_symbol.lower() == 'sugar':
        return 'data/historical/sugar/'
    if market_symbol.lower() == 'swiss_franc':
        return 'data/historical/swiss_franc/'
    if market_symbol.lower() == 't_bills':
        return 'data/historical/t_bills/'
    if market_symbol.lower() == 'wheat':
        return 'data/historical/wheat/'
    if market_symbol.lower() == 'yen':
        return 'data/historical/yen/'
    if market_symbol.lower() == 'canadian_dollar':
        return 'data/historical/canadian_dollar/'
    if market_symbol.lower() == 'error_test':
        return 'data/error_tests/'
    if market_symbol.lower() == 'test_data':
        return 'data/test_data/'
    return